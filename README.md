# TEST ENTRYPOINT POC

Just a poc see if running a docker container with `--entrypoint` is possible

uses `--mount` to mount a volume and pass the executable files to the container

uses `--entrypoint` to overwrite the image's default entrypoint

```
> chmod +x /Full/localPath/to/project/test-docker-entrypoint/custom_entrypoint.sh

> docker run --mount type=bind,source=/Full/localPath/to/project/test-docker-entrypoint,target=/app/test-docker-entrypoint --entrypoint=/app/test-docker-entrypoint/custom_entrypoint.sh --rm -it  -e ENV1=MI_VAR_1 alpine

```

This should print:

```
Running custom entrypoint POC
Printing ENV vars:
ENV1: MI_VAR_1
```